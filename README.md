Never Sated

Initial Alpha Release 0.1.0

Release Notes:


New Features:

-Basic Functionality started
-Player can move between rooms
-Player can eat NPCs
-Digestion of NPCs implemented
-Spawning of NPCs implemented
-NPCs will spawn in random rooms
-NPCs will struggle when consumed and may escape again
-NPCs wonder between rooms


Known Bugs:

-So many, oh dear god so so many
-Player's stomach fullness will occasionally dip below zero when digestion is finishing up
-Game is excessively verbose
-Can see what NPCs are doing even when they are not in the same room


Future Releases:

-NPCs get names
-More rooms (Always more rooms)
-And loads more